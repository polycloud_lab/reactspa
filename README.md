# ReactSPA

A simple React SPA app to discover some awesome stuff !

## HOW TO guide

### Goals

Create a simple website with basic HTML and CSS.
And see how you can make your website portable, and how to deploy your application to the web easily.
Also you will see how we can automize the deployment process.

### Steps

**1. Clone the repository:**

```
git clone https://gitlab.com/polycloud_lab/flatsite.git <NAME_OF_YOUR_WEBSITE>
```

Open your new repository in your favorite IDE.

**2. Customize some HTML and CSS if wanted.**

Open the `src` folder and try to edit some files to customize your website.
See below how to run the application.

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-1-website
```

**3. Make a Dockerfile to containerize the website.**

Now you want to make your application portable.
You will need to containerize your website. You can look at [this](https://www.docker.com/blog/how-to-use-the-official-nginx-docker-image/) guide to do it.

Don't forget to build the app before you can run it ;)

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-2-container
```

**4. Make a GitLab-ci to deploy the app on CleverCloud.**

You now want to deploy the app on [CleverCloud](https://www.clever-cloud.com/fr/) (an awesome french cloud provider).
To automize this step, you want to deploy your application using [gitlab-ci](https://www.youtube.com/watch?v=ljth1Q5oJoo).

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-3-gitlab-ci
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
