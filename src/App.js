import './App.css';
import AlertMessage from './components/alert-message';
import Footer from './components/footer';
import Navbar from './components/navbar';
import Stats from './components/stats';
import Skills from './components/skills';

function App() {
  return (
    <>
      <Navbar />
      <div className="container is-fullhd">
        <AlertMessage>This website is under development for now !</AlertMessage>
        <section className="hero is-primary">
          <div className="hero-body">
            <h1 className="title">About me</h1>
            <p className="subtitle">
              My name is <strong>Bob</strong>, a young developer aiming to be a
              great DevOps engineer !
            </p>
          </div>
        </section>
        <Stats />
        <Skills />
      </div>
      <Footer name="Bob" />
    </>
  );
}

export default App;
